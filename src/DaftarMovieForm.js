import React, {useContext, useState, useEffect} from "react"
import axios from "axios"
import {DaftarMovieContext} from "./DaftarMovieContext"

const DaftarMovieForm = () =>{
  const [daftarMovie, setDaftarMovie] = useContext(DaftarMovieContext)
  const [input, setInput] = useState({title: "", duration: 0, rating: 0, genre: "", description: ""})

  useEffect(()=>{
    if (daftarMovie.statusForm === "changeToEdit"){
      let dataMovie = daftarMovie.lists.find(x=> x.id === daftarMovie.selectedId)
      setInput({title: dataMovie.title, duration: dataMovie.duration, genre: dataMovie.genre, rating: dataMovie.rating, description: dataMovie.description})
      setDaftarMovie({...daftarMovie, statusForm: "edit"})
    }
  },[daftarMovie])

  const handleChange = (event) =>{
    let typeOfInput = event.target.title

    switch (typeOfInput){
      case "title":
      {
        setInput({...input, title: event.target.value});
        break
      }
      case "rating":
      {
        setInput({...input, rating: event.target.value});
        break
      }
      case "duration":
      {
        setInput({...input, duration: event.target.value});
          break
      }
      case "genre":
      {
        setInput({...input, genre: event.target.value});
          break
      }
      case "description":
      {
        setInput({...input, description: event.target.value});
          break
      }
    default:
      {break;}
    }
  }
  
  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let title = input.title
    // let rating = input.rating.toString()
    // let duration = input.duration.toString()
    let description = input.description.toString()
    let genre = input.genre.toString()

    if (title.replace(/\s/g,'') !== "" && genre.replace(/\s/g,'' && description.replace(/\s/g,'') !== "") !== ""){      
      if (daftarMovie.statusForm === "create"){        
        axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title: input.title, genre: input.genre, rating: input.rating, duration: input.duration, description: input.description})
        .then(res => {
            setDaftarMovie(
              {statusForm: "create", selectedId: 0,
              lists: [
                ...daftarMovie.lists, 
                { id: res.data.id, 
                  title: input.title, 
                  genre: input.genre, 
                  rating: input.rating, 
                  duration: input.duration, 
                  description: input.description
                }]
              })
        })
      }else if(daftarMovie.statusForm === "edit"){
        axios.put(`http://backendexample.sanbercloud.com/api/movies/${daftarMovie.selectedId}`, {title: input.title, genre: input.genre, rating: input.rating, duration: input.duration, description: input.description})
        .then(() => {
            let dataMovie = daftarMovie.lists.find(el=> el.id === daftarMovie.selectedId)
            dataMovie.title = input.title
            dataMovie.duration = input.duration
            dataMovie.rating = input.rating
            dataMovie.genre = input.genre
            dataMovie.description = input.description
            setDaftarMovie({statusForm: "create", selectedId: 0, lists: [...daftarMovie.lists]})
        })
      }

      setInput({title: "", duration: 0, rating: 0, genre: "", description: ""})
    }

  }
  return(
    <>
      <h1>Form Pengisian Movie</h1>

      <div style={{width: "50%", margin: "0 auto", display: "block"}}>
        <div style={{border: "1px solid #aaa", padding: "20px"}}>
          <form onSubmit={handleSubmit}>
            <label style={{float: "left"}}>
              Title:
            </label>
            <input style={{float: "right"}} type="text" name="title" value={input.title} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Durasi:
            </label>
            <input style={{float: "right"}} type="number" name="duration" value={input.duration} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Rating:
            </label>
            <input style={{float: "right"}} type="number" name="rating" value={input.rating} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Genre:
            </label>
            <input style={{float: "right"}} type="text" name="genre" value={input.genre} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Deskripso:
            </label>
            <input style={{float: "right"}} type="text" name="description" value={input.description} onChange={handleChange}/>
            <br/>
            <br/>
            <div style={{width: "100%", paddingBottom: "20px"}}>
              <button style={{ float: "right"}}>submit</button>
            </div>
          </form>
        </div>
      </div>
    </>
  )
}

export default DaftarMovieForm
