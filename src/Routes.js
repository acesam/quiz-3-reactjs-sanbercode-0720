import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from './Home';
import About from './About';
import DaftarMovie from './DaftarMovie';
import Nav from './Nav';
import { ThemeProvider } from "./ThemeContext";

// import Admin from "./pages/Admin";
// import { AuthContext } from "./context/auth";
// import PrivateRoute from './PrivateRoute';

export default function App() {

    // const existingTokens = JSON.parse(localStorage.getItem("tokens"));
    // const [authTokens, setAuthTokens] = useState(existingTokens);
    
    // const setTokens = (data) => {
    //     localStorage.setItem("tokens", JSON.stringify(data));
    //     setAuthTokens(data);
    // }
      
  return (
      <>
        <ThemeProvider>          
          <Nav/>
          <Switch>
          {/* <AuthContext.Provider value={false}> */}
            <Route exact path="/">
              <Home />
            </Route>

            <Route path="/about">
              <About start={200} />
            </Route>
            
            <Route path="/movielist">
              <DaftarMovie />
            </Route>

            {/* <PrivateRoute path="/admin" component={Admin} />
          </AuthContext.Provider> */}
          </Switch>
        
        </ThemeProvider>
      </>
  );
}
