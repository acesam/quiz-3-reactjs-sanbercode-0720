import React, {useContext, useEffect} from "react"
import axios from "axios"
import {DaftarMovieContext} from "./DaftarMovieContext"

const DaftarMovieList = () =>{

  const [daftarMovie, setDaftarMovie] = useContext(DaftarMovieContext)

  useEffect( () => {
    if (daftarMovie.lists === null){
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        setDaftarMovie({
          ...daftarMovie, 
          lists: res.data.map(el=>{ 
            return {id: el.id,
            title: el.title, 
            genre: el.genre, 
            rating: el.rating, 
            duration: el.duration, 
            description: el.description 
            }
          })
        })
      })
    }
  }, [setDaftarMovie]) // eslint-disable-line react-hooks/exhaustive-deps

  const handleEdit = (event) =>{
    let idDataMovie = parseInt(event.target.value)
    setDaftarMovie({...daftarMovie, selectedId: idDataMovie, statusForm: "changeToEdit"})
  }

  const handleDelete = (event) => {
    let idDataMovie = parseInt(event.target.value)

    let newLists = daftarMovie.lists.filter(el => el.id !== idDataMovie)

    axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idDataMovie}`)
    .then(res => {
      console.log(res)
    })
          
    setDaftarMovie({...daftarMovie, lists: [...newLists]})
    
  }

  return(
    <>
      <h1>List-list Movie Terkini</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Duration</th>
            <th>Rating</th>
            <th>Genre</th>
            <th>Deskripso</th>
          </tr>
        </thead>
        <tbody>

            {
              daftarMovie.lists !== null && daftarMovie.lists.map((item, index)=>{
                return(                    
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.title}</td>
                    <td>{item.duration}</td>
                    <td>{item.rating} </td>
                    <td>{item.genre} </td>
                    <td>{item.description} </td>
                    <td>
                      <button onClick={handleEdit} value={item.id}>Edit</button>
                      &nbsp;
                      <button onClick={handleDelete} value={item.id}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
        </tbody>
      </table>      
    </>
  )
}

export default DaftarMovieList
