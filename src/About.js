import React from 'react';

class Nama extends React.Component {
  render() {
    return <h1>Nama Peserta : {this.props.nama}</h1>;
  }
}

class Email extends React.Component {
  render() {
    return <h1>Email Peserta : {this.props.email}</h1>;
  }
}

class Os extends React.Component {
    render() {
      return <h1>Sistem Operasi yang Digunakan : {this.props.os}</h1>;
    }
}

class Gitlab extends React.Component {
    render() {
      return <h1>Akun Gitlab : {this.props.gitlab}</h1>;
    }
}

class Telegram extends React.Component {
    render() {
        return <h1>Akun Telegram : {this.props.tele}</h1>;
    }
}
var peserta = [
  {nama: "Cleoputra Goldi", email: "abdigoldi@gmail.com", os: "Windows", gitlab: "cleoputraa", tele: "Cleo Putra"},

]

class About extends React.Component {
  render() {
    return (
      <>
        {peserta.map(el=> {
          return (
            <div style={{border: "1px solid #000", padding: "20px"}}>
              <Nama nama={el.nama}/> 
              <Email email={el.email}/>
              <Os os={el.os}/> 
              <Gitlab gitlab={el.gitlab}/> 
              <Telegram tele={el.tele}/>  
            </div>
          )
        })}
      </>
    )
  }
}

export default About